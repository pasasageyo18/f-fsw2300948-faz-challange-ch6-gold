"use strict";

const { sequelize } = require("../models");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    // ALLOWING TO PERFORM MANY SEQUELIZE OPERATIONS ALL AT ONCE
    const transaction = await sequelize.transaction();

    try {
      // USER_GAME DATA
      const userGameData = [
        {
          uuid: "169cd74e-e72c-4dcf-b2e7-46d5b36b4d91",
          username: "darling",
          password: "superadmin",
          role: "superadmin",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
      ];

      // INSERT THE DATA TO DATABASE
      await queryInterface.bulkInsert("user_games", userGameData, {
        transaction,
      });

      // USER_GAME_BIODATA DATA
      const userGameBioData = [
        {
          name: "john Doe",
          userGameId: 1,
          email: "john@gmail.com",
          gender: "male",
          dateOfBirth: "2012-02-07",
          city: "jakarta",
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
      ];

      // INSERT THE DATA TO DATABASE
      await queryInterface.bulkInsert("user_game_biodata", userGameBioData, {
        transaction,
      });

      // USER_GAME_HISTORY DATA
      const userGameHistoryData = [
        {
          userGameHistoryId: 1,
          win: 1,
          lose: 0,
          timePlay: "2023-07-19T08:49:52.097Z",
          totalMatch: 1,
          createdAt: "2023-07-19T07:27:43.155Z",
          updatedAt: "2023-07-19T08:49:52.097Z",
        },
      ];

      // INSERT THE DATA TO DATABASE
      await queryInterface.bulkInsert(
        "user_game_history",
        userGameHistoryData,
        { transaction }
      );

      // COMMIT THE CHANGES
      await transaction.commit();
    } catch (err) {
      // UNDO THE CHANGE IF THERE'S AN ERROR
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games", null, {});
    await queryInterface.bulkDelete("user_game_biodata", null, {});
    await queryInterface.bulkDelete("user_game_history", null, {});
  },
};
