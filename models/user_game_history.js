"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_game_history.belongsTo(models.User_game, {
        foreignKey: "userGameHistoryId",
        as: "historyId",
      });
    }
  }
  User_game_history.init(
    {
      uuid: {
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
      },
      userGameHistoryId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "user_games",
          key: "id",
        },
      },
      win: {
        type: DataTypes.INTEGER,
      },
      lose: {
        type: DataTypes.INTEGER,
      },
      timePlay: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      totalMatch: {
        type: DataTypes.INTEGER,
      },
    },
    {
      sequelize,
      modelName: "User_game_history",
      tableName: "user_game_history",
    }
  );
  return User_game_history;
};
