// GET THE TRIAL PAGE
const getTrial = (req, res) => {
  res.render("trial");
};

// EXPORT ALL THE FUNCTIONS
exports.getTrial = getTrial;
