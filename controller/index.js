// GET THE INDEX PAGE
const getIndex = (req, res) => {
  res.render("index");
};

// EXPORT ALL THE FUNCTIONS
exports.getIndex = getIndex;
