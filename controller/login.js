const { User_game } = require("../models");

// GET THE LOGIN FORM
const getLogin = (req, res) => {
  res.render("login");
};

// POST THE REQUESTED DATA TO BODY AND VERIFY THE USER
const postLogin = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User_game.findOne({ where: { username: username } });
    if (!user)
      return res.status(400).json({ msg: "Username couldn't be found!" });

    const passwordMatch = user.password;
    if (password !== passwordMatch)
      return res.status(400).json({ msg: "Password is false!" });

    const roleMatch = user.role;
    if (roleMatch !== "superadmin")
      return res.status(400).json({ msg: "Only superadmin can login!" });

    // IF ALL CLEAR THE PAGE WILL REDIRECT TO DASHBOARD
    res.redirect("/dashboard-user-game-history");
  } catch (err) {
    return res.status(400).json({ msg: err });
  }
};

// EXPORT ALL THE FUNCTIONS
exports.getLogin = getLogin;
exports.postLogin = postLogin;
