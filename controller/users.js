const { User_game, User_game_biodata } = require("../models");

// GET THE FORM TO CREATE USER AND BIO DATA
const getCreate = (req, res) => {
  res.render("create");
};

// REQUEST AND POST THE BIO AND USER DATA TO THE DATABASE
const postCreate = async (req, res) => {
  try {
    const { username, password, role, name, email, gender, dateOfBirth, city } =
      req.body;
    const user = await User_game.create({
      username,
      password,
      role,
    });

    const biodata = await User_game_biodata.create({
      userGameId: user.id,
      name,
      email,
      gender,
      dateOfBirth,
      city,
    });

    res.redirect("/dashboard-user-game");
  } catch (err) {
    console.error(err);
  }
};

// GET THE FORM TO UPDATE USER AND PASS THE EXISTING DATA TO THE FORM VALUE
const getFormUpdate = (req, res) => {
  User_game.findOne({ where: { id: req.params.id } }).then((user) => {
    res.render("update", {
      id: user.id,
      username: user.username,
      password: user.password,
      role: user.role,
    });
  });
};

// POST THE UPDATED USER DATA TO DATABASE
const updateUser = (req, res) => {
  User_game.update(
    {
      username: req.body.username,
      password: req.body.password,
      role: req.body.role,
    },
    {
      where: { id: req.params.id },
    }
  )
    .then((user) => {
      console.log("user successfully updated!");
      res.redirect("/dashboard-user-game");
    })
    .catch((err) => {
      res.status(500).json("Can't update an user", err);
    });
};

// DELETE THE USER DATA
const deleteUser = (req, res) => {
  User_game.destroy({ where: { id: req.params.id } }).then((user) => {
    res.render("delete");
  });
};

// EXPORT ALL THE FUNCTIONS
exports.getCreate = getCreate;
exports.postCreate = postCreate;
exports.getFormUpdate = getFormUpdate;
exports.updateUser = updateUser;
exports.deleteUser = deleteUser;
