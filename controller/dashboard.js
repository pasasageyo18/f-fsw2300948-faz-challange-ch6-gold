const {
  User_game,
  User_game_biodata,
  User_game_history,
} = require("../models");

// GET ALL OF THE USER_GAME AND PASS IT TO THE EJS FILE
const getDashboardUser = (req, res) => {
  User_game.findAll().then((users) => {
    res.render("dashboard-user-game", {
      users,
    });
  });
};

// GET ALL OF THE BIODATA ATTRIBUTES IN THE USER GAME AND PASS IT TO THE EJS FILE
const getDashboardUserBio = async (req, res) => {
  try {
    const biodata = await User_game.findAll({
      include: [
        {
          model: User_game_biodata,
          as: "biodata",
          attributes: [
            "id",
            "uuid",
            "userGameId",
            "name",
            "email",
            "gender",
            "dateOfBirth",
            "city",
          ],
        },
      ],
    });
    res.render("dashboard-user-game-biodata", { biodata });
  } catch (err) {
    res.status(500).json(err);
  }
};

// GET ALL OF THE HISTORY IN THE USER GAME AND PASS IT TO THE EJS FILE
const getDashboardUserHistory = async (req, res) => {
  try {
    const history = await User_game_history.findAll({
      include: { model: User_game, as: "historyId", attributes: ["id"] },
    });

    res.render("dashboard-user-game-history", { history });
  } catch (err) {
    console.error(err);
  }
};

// EXPORT ALL THE FUNCTIONS
exports.getDashboardUser = getDashboardUser;
exports.getDashboardUserBio = getDashboardUserBio;
exports.getDashboardUserHistory = getDashboardUserHistory;
