const express = require("express");
const router = express.Router();
const login = require("../controller/login");

// READ
router.get("/login", login.getLogin);

// CREATE
router.post("/login", login.postLogin);

module.exports = router;
