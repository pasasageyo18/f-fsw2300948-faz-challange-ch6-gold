const express = require("express");
const router = express.Router();
const trial = require("../controller/trial");

// READ
router.get("/trial", trial.getTrial);

module.exports = router;
