const express = require("express");
const router = express.Router();
const dashboard = require("../controller/dashboard");

// READ
router.get("/dashboard-user-game", dashboard.getDashboardUser);
router.get("/dashboard-user-game-biodata", dashboard.getDashboardUserBio);
router.get("/dashboard-user-game-history", dashboard.getDashboardUserHistory);

module.exports = router;
