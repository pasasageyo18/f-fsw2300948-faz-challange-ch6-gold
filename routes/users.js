const express = require("express");
const router = express.Router();
const user = require("../controller/users");

// CREATE
router.post("/dashboard-user-game/create-new", user.postCreate);

// READ
router.get("/dashboard-user-game/create-new-user", user.getCreate);
router.get("/dashboard-user-game/update/(:id)", user.getFormUpdate);

// UPDATE
router.post("/dashboard-user-game/update/(:id)", user.updateUser);

// DELETE
router.get("/dashboard-user-game/delete/(:id)", user.deleteUser);

module.exports = router;
