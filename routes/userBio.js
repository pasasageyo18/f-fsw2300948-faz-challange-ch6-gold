const express = require("express");
const router = express.Router();
const userBio = require("../controller/userBio");

// READ
router.get(
  "/dashboard-user-game-biodata/create-new-user-bio",
  userBio.getFormUpdateBio
);
router.get(
  "/dashboard-user-game-biodata/update/(:id)",
  userBio.getFormUpdateBio
);

//UPDATE
router.post("/dashboard-user-game-biodata/update/(:id)", userBio.updateBio);

module.exports = router;
