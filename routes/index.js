const express = require("express");
const router = express.Router();
const index = require("../controller/index");

// READ
router.get("/", index.getIndex);

module.exports = router;
